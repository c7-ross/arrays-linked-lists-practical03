//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>





int main (int argc, const char* argv[])
{
    float *floatPoint = nullptr; //declare pointer
    
    floatPoint = new float; //pointer now points to float on the heap
    
    if (floatPoint != nullptr)
    {
        *floatPoint = 10;
    }
    
    std::cout << "Value is " << *floatPoint << "\n";
    
    delete floatPoint;
    
    return 0;
}

